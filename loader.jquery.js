$.fn.iconloader = function(options){
  options = options || {};
  var opt = {
    icons: 'assets/icons/',
    custom: {}
  };
  var $style = $('<style></style>');

  var option_keys = Object.keys(options);
  for(var i=0;i<option_keys.length;i++){
    opt[option_keys[i]] = options[option_keys[i]];
  }
  var getUrl = function(icon){
    return opt.icons+icon+'.svg';
  }
  var getCustomSelector = function(icon){
    if(typeof opt.custom[icon] == typeof undefined || !opt.custom[icon].length ){
      return '';
    }
    return opt.custom[icon].join(',')+',';
  }

  var buildicons = function(){
    var icons = Object.keys(opt.custom);
    $('*[data-icon]').each(function(){
      var icon = $(this).attr('data-icon');
      if($(this).is('[data-icon-embed]')){
        $(this).load(getUrl(icon));
      }else if(icons.indexOf(icon) < 0){
        icons.push(icon);
      }
    });

    var css = ['[data-icon]{background-repeat:no-repeat;}'];
    $.each(icons,function(k,v){
      css.push(getCustomSelector(v)+'[data-icon='+v+'],[data-icon='+v+']:before{background-image:url('+getUrl(v)+')}')
    });
    css.push('[data-icon-embed],[data-icon-embed]:before{background-image:none;}')
    $style.text(css.join(''));
    $style.appendTo($('head'));
  }

  var rebuild = function(){
    $style.remove();
    $style = $('<style></style>');
    buildicons();
  }

  $(function(){
    buildicons()
  });

  return {
    rebuild: rebuild
  }
}
